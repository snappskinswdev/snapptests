var cp = require('child_process'), server;

module.exports.run = function () {return new Promise(function (resolve, reject) {
  server = cp.spawn('java', ['-jar', 'selenium-server-standalone.jar']);

  server.stdout.on('data', function (data) {});

  server.stderr.on('data', function (data) {
      if (/Started SocketListener/ig.test(data.toString())) {
        console.log('server started');
        resolve()
      }
  });
})};

module.exports.shutdown = function () {return new Promise(function (resolve, reject) {
  server.on('close', function (code, signal) {
    resolve();
  });

  server.kill('SIGTERM');
})};