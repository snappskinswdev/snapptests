var server = require('./server.js'),
    argv = require('optimist').argv,
    assert = require("assert"),
    config = require('./configureEnvironment'),
    myClient = require("./helpers/client");

function importTest (name, path) {
  describe(name, function () {
      require(path);
  });
}

global.HELPERS_PATH = __dirname + '/helpers';
global.loadHelper = function (helperName) {
    return require(global.HELPERS_PATH + '/' + helperName);
};

require('./test/_beforeAllTests');
require('./test/_afterAllTests');

describe('Snappskin tests', function () {
    this.timeout(99999999);

    /**ok tests**/
    importTest('sensorType', './test/sensorType');
    importTest('technicalSupport', './test/technicalSupport');
    //importTest('snaType', './test/snaType');
    //importTest('command', './test/command');
    //importTest('assets', './test/assets');
    //importTest('legal', './test/legal');
    //importTest('carePlan', './test/carePlan');
    //importTest('registration', './test/registration');
    //importTest('project', './test/project');

    /**broken tests**/
    //importTest('roles', './test/roles');
    //importTest('account', './test/account');
    //importTest('patients', './test/patients');
    //importTest('accessGroup', './test/accessGroup');
    //importTest('protocols', './test/protocols');

});