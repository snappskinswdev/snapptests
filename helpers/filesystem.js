/**
 * Created by p.hudinsky on 9/17/2015.
 */
var fs = require('fs');

module.exports = fs;

fs.removeDirSync = function (dirPath, removeSelf, withoutFiles) {
    withoutFiles = withoutFiles || [];

    if (removeSelf === undefined)
        removeSelf = true;
    try {
        var files = fs.readdirSync(dirPath);
    }
    catch (e) {
        return;
    }
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (withoutFiles.indexOf(file) !== -1) {
                continue;
            }
            var filePath = dirPath + '/' + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
            else
                fs.removeDirSync(filePath);
        }
    if (removeSelf)
        fs.rmdirSync(dirPath);
};