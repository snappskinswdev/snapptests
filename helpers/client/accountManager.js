/**
 * Created by p.hudinsky on 9/22/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var
    firstNameInputName = 'first_name',
    lastNameInputName = 'last_name',
    phoneInputName = 'phone',
    emailInputName = 'email',
    usernameInputName = 'username',
    passwordInputName = 'password',
    passwordConfirmInputName = 'password_confirm',
    employeeNumberInputName = 'employee_num'
    ;

module.exports.extend = function (client) {
    client.snapp.account = {};

    client.snapp.account.generateProtocolSelectString = function(protocolOptions) {
        return protocolOptions.number + " - " + protocolOptions.name;
    };


    client.snapp.account.fillAddAccountPopup = function(data, cb){
        data.firstName = data.firstName || unique.generate('String_');
        data.lastName = data.lastName || unique.generate('String_');
        data.username = data.username || unique.generate('String_');
        data.password = data.password ||unique.generate('SnappSkin&1_');
        data.passwordConfirm = data.passwordConfirm || data.password;
        data.phone = data.phone || unique.generatePhone();
        data.email = data.email || unique.generateEmail();

        if (data.firstName) client.setTextInputValueByName(firstNameInputName, data.firstName);
        if (data.lastName) client.setTextInputValueByName(lastNameInputName, data.lastName);
        if (data.username) client.setTextInputValueByName(usernameInputName, data.username);
        if (data.phone) client.setTextInputValueByName(phoneInputName, data.phone);
        if (data.email) client.setTextInputValueByName(emailInputName, data.email);
        if (data.employeeNumber) client.setTextInputValueByName(employeeNumberInputName, data.employeeNumber);
        if (data.password) client.setTextInputValueByName(passwordInputName, data.password);
        if (data.passwordConfirm) client.setTextInputValueByName(passwordConfirmInputName, data.passwordConfirm);
        cb();
    };

    client.snapp.account.saveAddAccountPopup = function(cb){
        client.savePopup(function(){
            client.waitForSuccessPopupAppeared(cb)
        });
    };

    client.snapp.account.create = function (data, cb) {
        data.firstName = data.firstName || unique.generate('String_');
        data.lastName = data.lastName || unique.generate('String_');
        data.username = data.username || unique.generate('String_');
        data.password = data.password ||unique.generate('SnappSkin&1_');
        data.passwordConfirm = data.passwordConfirm || data.password;
        data.phone = data.phone || unique.generatePhone();
        data.email = data.email || unique.generateEmail();

        client.clickOnMenu('#accounts', function () {
            client.waitForNewButton(function (res) {
                assert(res, "New button was not appeared.");

                client.clickOnNewButton(function () {
                    client.waitForPopupAppeared(function(){
                        client.snapp.account.fillAddAccountPopup(data, function(){
                            client.savePopup(function(){
                                client.waitForSuccessPopupAppeared(cb)
                            });
                        });
                    });
                })
            })
        })
    };

    client.snapp.account.openEditPage = function(lastName, cb) {

        client.clickOnMenu('#accounts', function() {
            client.waitForDataTableWasLoaded(function () {
                client.search(lastName, function () {
                    client.waitTableRowAndClickByText(lastName, function () {
                        client.clickOnEditButton(function () {
                            client.waitForPopupAppeared(cb);
                        })
                    })
                })
            })
        });
    };

    client.snapp.account.edit = function(lastName, data, cb) {
        data = data || {};

        client.snapp.account.openEditPage(lastName, function(){
            if (data.firstName) client.setTextInputValueByName(firstNameInputName, data.firstName);
            if (data.lastName) client.setTextInputValueByName(lastNameInputName, data.lastName);
            if (data.employeeNumber) client.setTextInputValueByName(employeeNumberInputName, data.employeeNumber);
            if (data.password) client.setTextInputValueByName(passwordInputName, data.password);
            if (data.passwordConfirm) client.setTextInputValueByName(passwordConfirmInputName, data.passwordConfirm);

            client.savePopup(function () {
                client.waitForSuccessPopupAppeared(function(){
                    client.clickOnBackButton(function(){
                        client.waitForDataTableWasLoaded(cb)
                    })
                });
            });
        })
    };

    client.snapp.account.checkValues = function(lastName, data, cb) {
        data = data || {};

        client.snapp.account.openEditPage(lastName, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(lastNameInputName, function (text) {
                    assert.equal(text, lastName, 'lastName was not saved.');
                    cb();
                });
            });
            if (data.firstName) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(firstNameInputName, function (text) {
                        assert.equal(text, data.firstName, 'firstName id was not saved.');
                        cb();
                    });
                });
            }
            if (data.phone) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(phoneInputName, function (text) {
                        var assertion = (text == data.phone) || (text.replace(/-/g, "") == data.phone.replace(/-/g, ""));
                        client.assert(assertion, 'phone was not saved.');
                        cb();
                    });
                });
            }
            if (data.email) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(emailInputName, function (text) {
                        assert.equal(text, data.email, 'email was not saved.');
                        cb();
                    });
                });
            }
            if (data.username) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(usernameInputName, function (text) {
                        assert.equal(text, data.username, 'username was not saved.');
                        cb();
                    });
                });
            }
            if (data.employeeNumber) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(employeeNumberInputName, function (text) {
                        assert.equal(text, data.employeeNumber, 'employeeNumber was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.cancelPopup(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.account.tryToDelete = function(lastName, cb) {
        client.clickOnMenu('#accounts', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndTryToDeleteTableRowWithText(lastName, cb);
            });
        })
    };

    client.snapp.account.delete = function (lastName, cb) {
        client.clickOnMenu('#accounts', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndDeleteTableRowWithText(lastName, cb);
            });
        })
    };

    return client;
};
