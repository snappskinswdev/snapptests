/**
 * Created by p.hudinsky on 9/22/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

module.exports.extend = function (client) {
    client.snapp.asset = {};

    client.snapp.asset.formElementNames = {
        releasedDate: 'released_date',
        description: 'description',
        file: 'file'

    };
    var formElementNames = client.snapp.asset.formElementNames;

    client.snapp.asset.openPage = function (cb) {
        client.clickOnMenu("#assets", function () {
            client.waitForDataTableWasLoaded(cb);
        });
    };

    client.snapp.asset.openSnaSoftPage = function (cb) {
        client.snapp.asset.openPage(function () {
            client.clickOnMenu("button[data-action='application']", function(){
                client.pause(1000);
                client.waitForDataTableWasLoaded(cb);
            })
        });
    };

    client.snapp.asset.openSnaConfigPage = function (cb) {
        client.snapp.asset.openPage(function () {
            client.clickOnMenu("button[data-action='config']", function(){
                client.pause(1000);
                client.waitForDataTableWasLoaded(cb);
            })
        });
    };

    client.snapp.asset.checkIfSnaSoftUploaded = function (fileName, cb) {
        client.snapp.asset.openSnaSoftPage(function () {
            client.snapp.common.searchAndCheckIsTableRowWithTextExist(fileName, cb);
        });
    };

    client.snapp.asset.tryToUploadSnaSoft = function (data, cb) {
        client.snapp.asset.openSnaSoftPage(function () {
            client.clickOnUploadButton(function(){
                client.waitForPopupAppeared(function () {
                    client.snapp.asset.fillUploadPopup(data, function(){
                        client.savePopup(cb);
                    });
                })
            })
        });
    };

    client.snapp.asset.tryToUploadSnaConfig = function (data, cb) {
        client.snapp.asset.openSnaConfigPage(function () {
            client.clickOnUploadButton(function(){
                client.waitForPopupAppeared(function () {
                    client.snapp.asset.fillUploadPopup(data, function(){
                        client.savePopup(cb);
                    });
                })
            })
        });
    };

    client.snapp.asset.uploadSnaSoft = function (data, cb) {
        client.snapp.asset.tryToUploadSnaSoft(data, function(){
            client.waitForSuccessPopupAppeared(20000, function () {
                client.waitForDataTableWasLoaded(cb);
            });
        });
    };

    client.snapp.asset.uploadSnaConfig = function (data, cb) {
        client.snapp.asset.tryToUploadSnaConfig(data, function(){
            client.waitForSuccessPopupAppeared(20000, function () {
                client.waitForDataTableWasLoaded(cb);
            });
        });
    };

    client.snapp.asset.fillUploadPopup = function(data, cb){
        data.description = data.description || unique.generate('String_');
        data.releasedDate = data.releasedDate || unique.generateDate();

        if (data.description) client.setTextInputValueByName(formElementNames.description, data.description);
        if (data.file) client.chooseFileByInputName(formElementNames.file, data.file);

        client.isFormElementWithNameVisible(formElementNames.releasedDate, function(isVisible){
            if (data.releasedDate && isVisible) {
                client.setTextInputValueByName(formElementNames.releasedDate, data.releasedDate);
            }
            cb();
        });
    };

    client.snapp.asset.tryToDeleteSnaSoft = function(fileName, cb) {
        client.snapp.asset.openSnaSoftPage(function () {
            client.snapp.common.searchAndTryToDeleteTableRowWithText(fileName, cb);
        });
    };

    client.snapp.asset.deleteSnaSoft = function (fileName, cb) {
        client.snapp.asset.openSnaSoftPage(function () {
            client.snapp.common.searchAndDeleteTableRowWithText(fileName, cb);
        });
    };

    client.snapp.asset.tryToDeleteSnaConfig = function(description, cb) {
        client.snapp.asset.openSnaConfigPage(function () {
            client.snapp.common.searchAndTryToDeleteTableRowWithText(description, cb);
        });
    };

    client.snapp.asset.deleteSnaConfig = function (description, cb) {
        client.snapp.asset.openSnaConfigPage(function () {
            client.snapp.common.searchAndDeleteTableRowWithText(description, cb);
        });
    };

    return client;
};
