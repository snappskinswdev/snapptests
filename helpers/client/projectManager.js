/**
 * Created by p.hudinsky on 5/20/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = require("../unique");

module.exports.extend = function (client) {
    client.snapp.project = {};

    client.snapp.project.formElementNames = {
        protocol: 'protocol',
        rxNumber: 'rx_number',
        description: 'description',
        startDateTime: 'start_date_time',
        periodData: 'period_data',
        endDateTime: 'end_date_time'
    };
    var formElementNames = client.snapp.project.formElementNames;

    client.snapp.project.openPage = function (cb) {
        client.clickOnMenu("#projects", function () {
            client.waitForDataTableWasLoaded(cb);
        });
    };

    client.snapp.project.create = function(data, cb) {
        data.rxNumber = data.rxNumber || unique.generate('Rx_');
        data.description = data.description || unique.generate('Description_');
        data.startDateTime = data.startDateTime || unique.generateDate();
        data.periodData = data.periodData || unique.generateNumbers(2);

        client.snapp.project.openPage(function(){
            client.clickOnNewButton(function () {
                client.waitForPopupAppeared(function() {
                    client.snapp.project.fillAddProjectPopup(data, function(){
                        client.snapp.project.saveAddProjectPopup(function () {
                            client.snapp.project.waitForProjectEditPage(function() {
                                client.clickOnBackButton(function () {
                                    client.waitForDataTableWasLoaded(cb);
                                });
                            });
                        });
                    });
                });
            });
        });
    };

    client.snapp.project.fillAddProjectPopup = client.snapp.project.fillEditForm = function(data, cb){
        data.rxNumber = data.rxNumber || unique.generate('Rx_');
        data.description = data.description || unique.generate('Description_');
        data.startDateTime = unique.generateDate();
        data.periodData = unique.generateNumbers(2);

        client.select(formElementNames.protocol, data.protocolString);
        client.setTextInputValueByName(formElementNames.rxNumber, data.rxNumber);
        client.setTextInputValueByName(formElementNames.description, data.description);
        client.setTextInputValueByName(formElementNames.startDateTime, data.startDateTime);
        client.setTextInputValueByName(formElementNames.periodData, data.periodData);

        client.isFormElementWithNameVisible(formElementNames.endDateTime, function(isVisible){
            if (data.endDateTime && isVisible) {
                client.setTextInputValueByName(formElementNames.endDateTime, data.endDateTime);
            }
            cb();
        });
    };

    client.snapp.project.saveAddProjectPopup = function(cb){
        client.clickOnSavePopupButton(function(){
            client.snapp.project.waitForProjectEditPage(cb);
        });
    };

    client.snapp.project.saveEditForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(cb);
        });
    };

    client.snapp.project.openEditPage = function(rxNumber, cb) {
        client.snapp.project.openPage(function() {
            client.search(rxNumber, function () {
                client.waitTableRowAndClickByText(rxNumber, function () {
                    client.clickOnEditButton(function () {
                        client.snapp.project.waitForProjectEditPage(cb);
                    })
                })
            })
        });
    };

    client.snapp.project.edit = function(rxNumber, data, cb) {
        data = data || {};

        client.snapp.project.openEditPage(rxNumber, function(){
            client.snapp.project.fillEditForm(data, function () {
                client.snapp.project.saveEditForm(function () {
                    client.clickOnBackButton(function(){
                        client.waitForDataTableWasLoaded(cb)
                    })
                });
            });
        })
    };

    client.snapp.project.checkValues = function(rxNumber, data, cb) {
        data = data || {};

        client.snapp.project.openEditPage(rxNumber, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(formElementNames.rxNumber, function (text) {
                    assert.equal(text, rxNumber, 'rxNumber was not saved.');
                    cb();
                });
            });
            if (data.startDateTime) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.startDateTime, function (text) {
                        assert.equal(text, data.startDateTime, 'startDateTime id was not saved.');
                        cb();
                    });
                });
            }
            if (data.periodData) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.periodData, function (text) {
                        assert.equal(text, data.periodData, 'periodData id was not saved.');
                        cb();
                    });
                });
            }
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.description, function (text) {
                        assert.equal(text, data.description, 'description id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.project.tryToDelete = function(number, cb) {
        client.clickOnMenu('#projects', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndTryToDeleteTableRowWithText(number, cb);
            });
        })
    };

    client.snapp.project.delete = function (number, cb) {
        client.clickOnMenu('#projects', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndDeleteTableRowWithText(number, cb);
            });
        })
    };

    client.snapp.project.deleteArchived = function (number, cb) {
        client.clickOnMenu('#projects', function () {
            client.waitForDataTableWasLoaded(function () {
                client.clickOnShowArchivedButton(function(){
                    client.waitForDataTableWasLoaded(function () {
                        client.snapp.common.searchAndDeleteTableRowWithText(number, cb);
                    });
                });
            });
        })
    };

    client.snapp.project.waitForProjectEditPage = function (cb) {
        var message = 'Project edit view was not displayed';

        client.waitForExists('input[name="rx_number"]', 6000, function (res) {
            client.assert(res, message);

            client.waitForExists('#patient-data', 6000, function (res) {
                client.assert(res, message);
                cb();
            });
        });
    };

    return client;
};
