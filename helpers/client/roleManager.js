/**
 * Created by p.hudinsky on 9/24/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var
    nameInputName = 'name',
    descriptionInputName = 'description'
    ;

module.exports.extend = function (client) {
    client.snapp.role = {};

    client.snapp.role.fillAddRolePopup = function(data, cb){
        data.name = data.name || unique.generate('String_');
        data.description = data.description || unique.generate('String_');

        if (data.name) client.setTextInputValueByName(nameInputName, data.name);
        if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);
        cb();
    };

    client.snapp.role.saveAddRolePopup = function(cb){
        client.savePopup(function(){
            client.waitForSuccessPopupAppeared(cb)
        });
    };

    client.snapp.role.create = function (data, cb) {
        client.clickOnMenu('#roles', function () {
            client.waitForNewButton(function () {
                client.clickOnNewButton(function () {
                    client.waitForPopupAppeared(function(){
                        client.snapp.role.fillAddRolePopup(data, function(){
                            client.snapp.role.saveAddRolePopup(cb);
                        });
                    });
                })
            })
        })
    };

    client.snapp.role.openEditPage = function(name, cb) {

        client.clickOnMenu('#roles', function() {
            client.waitForDataTableWasLoaded(function () {
                client.search(name, function () {
                    client.waitTableRowAndClickByText(name, function () {
                        client.clickOnEditButton(function () {
                            client.waitForPopupAppeared(cb);
                        })
                    })
                })
            })
        });
    };

    client.snapp.role.edit = function(name, data, cb) {
        data = data || {};

        client.snapp.role.openEditPage(name, function(){
            if (data.name) client.setTextInputValueByName(nameInputName, data.name);
            if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);

            client.savePopup(function () {
                client.waitForSuccessPopupAppeared(function(){
                    client.clickOnBackButton(function(){
                        client.waitForDataTableWasLoaded(cb)
                    })
                });
            });
        })
    };

    client.snapp.role.checkValues = function(name, data, cb) {
        data = data || {};

        client.snapp.role.openEditPage(name, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(nameInputName, function (text) {
                    assert.equal(text, name, 'name was not saved.');
                    cb();
                });
            });
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(descriptionInputName, function (text) {
                        assert.equal(text, data.description, 'description id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.cancelPopup(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.role.tryToDelete = function(name, cb) {
        client.clickOnMenu('#roles', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndTryToDeleteTableRowWithText(name, cb);
            });
        })
    };

    client.snapp.role.delete = function (name, cb) {
        client.clickOnMenu('#roles', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndDeleteTableRowWithText(name, cb);
            });
        })
    };

    return client;
};
