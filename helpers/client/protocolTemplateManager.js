/**
 * Created by p.hudinsky on 9/23/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var nameInputName = 'template_name';
var descriptionInputName = 'template_description';
var periodDataInputName = 'period_data';

module.exports.extend = function (client) {
    client.snapp.protocolTemplate = {};

    client.snapp.protocolTemplate.fillAddTemplatePopup = function(data, cb){
        data.name = data.name || unique.generate('String_');
        data.description = data.description || unique.generate('String_');
        data.periodData = data.periodData || unique.generateNumbers(2);

        if (data.name) client.setTextInputValueByName(nameInputName, data.name);
        if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);
        if (data.periodData) client.setTextInputValueByName(periodDataInputName, data.periodData);
        cb();
    };

    client.snapp.protocolTemplate.saveAddTemplatePopup = function(cb){
        client.savePopup(function(){
            client.snapp.protocolTemplate.waitForTemplateEditPage(cb);
        });
    };

    client.snapp.protocolTemplate.waitForTemplateEditPage = function(cb){
        client.waitForExists('input[name="template_name"]', 6000, function (res) {
            client.assert(res, 'Protocol template edit view was not displayed.');
            cb();
        });
    };

    return client;
};
