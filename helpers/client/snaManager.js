/**
 * Created by p.hudinsky on 9/24/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var
    numberInputName = 'serial',
    descriptionInputName = 'description',
    locationInputName = 'location',
    typeSelectName = 'type_id'
    ;

module.exports.extend = function (client) {
    client.snapp.sna = {};

    client.snapp.sna.fillAddSnaPopup = function(data, cb){
        data.description = data.description || unique.generate('String_');
        data.location = data.location || unique.generate('String_');
        data.number = data.number || unique.generate('String_');

        client.select(typeSelectName, data.typeString);
        if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);
        if (data.location) client.setTextInputValueByName(locationInputName, data.location);
        if (data.number) client.setTextInputValueByName(numberInputName, data.number);
        cb();
    };

    client.snapp.sna.saveAddSnaPopup = function(cb){
        client.savePopup(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.waitForDataTableWasLoaded(cb)
            });
        });
    };

    client.snapp.sna.saveEditSnaForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.clickOnBackButton(function() {
                    client.waitForDataTableWasLoaded(cb)
                });
            });
        });
    };

    client.snapp.sna.create = function (data, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas"]', function () {
                client.waitForNewButton(function () {
                    client.clickOnNewButton(function () {
                        client.waitForPopupAppeared(function () {
                            client.snapp.sna.fillAddSnaPopup(data, function () {
                                client.snapp.sna.saveAddSnaPopup(cb);
                            });
                        });
                    })
                })
            })
        })
    };

    client.snapp.sna.openEditPage = function(number, cb) {

        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.search(number, function () {
                        client.waitTableRowAndClickByText(number, function () {
                            client.clickOnEditButton(function () {
                                client.waitForSaveButton(cb);
                            })
                        })
                    })
                })
            })
        });
    };

    client.snapp.sna.edit = function(number, data, cb) {
        data = data || {};

        client.snapp.sna.openEditPage(number, function(){
            client.snapp.sna.fillAddSnaPopup(data, function(){
                client.snapp.sna.saveEditSnaForm(cb);
            });
        })
    };

    client.snapp.sna.checkValues = function(number, data, cb) {
        data = data || {};

        client.snapp.sna.openEditPage(number, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(numberInputName, function (text) {
                    assert.equal(text, number, 'number was not saved.');
                    cb();
                });
            });
            if (data.location) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(locationInputName, function (text) {
                        assert.equal(text, data.location, 'location id was not saved.');
                        cb();
                    });
                });
            }
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(descriptionInputName, function (text) {
                        assert.equal(text, data.description, 'description id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.sna.tryToDelete = function(number, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndTryToDeleteTableRowWithText(number, cb);
                });
            })
        })
    };

    client.snapp.sna.delete = function (number, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndDeleteTableRowWithText(number, cb);
                });
            })
        })
    };

    return client;
};
