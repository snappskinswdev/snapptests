/**
 * Created by p.hudinsky on 5/20/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

module.exports.extend = function (client) {
    client.snapp.protocol = {};

    client.snapp.protocol.generateProtocolSelectString = function(protocolOptions) {
        return protocolOptions.number + " - " + protocolOptions.name;
    };

    client.snapp.protocol.create = function (options, callback) {
        var
            number = options['number'] || unique.generate("Number_"),
            description = options['description'] || unique.generate("Description_"),
            name = options['name'] || unique.generate("Name_")
            ;

        client.clickOnMenu('#protocols', function () {
            client.waitForNewButton(function (res) {

                client.clickOnNewButton(function () {

                    client.waitForExist('#save-button', 3000, false, function (err, res) {
                        client.assert(res, "Save button was not appeared.");

                        client
                            .setValue('[name="rx_type"]', number)
                            .setValue('[name="name"]', name)
                            .setValue('[name="description"]', description)
                            .click('#save-button', function () {

                                async.series(
                                    {
                                        "successPopup": function (cb) {
                                            client.waitForSuccessPopup(3000, false, function (err, res) {
                                                client.assert(res, "Protocol was saved with errors.");

                                                cb();
                                            })
                                        },
                                        "managerButton": function (cb) {
                                            client.waitForExists("#manage-button", 3000, function (res) {
                                                client.assert(res, "Manage button was not appeared.");

                                                cb();
                                            })
                                        }
                                    },
                                    function (err) {
                                        if (err) throw err;
                                        client.clickOnBackButton(function () {
                                            client.waitForDataTableWasLoaded(callback)
                                        });
                                    }
                                );
                            })
                    })
                })
            })
        })
    };

    client.snapp.protocol.openEditPage = function(number, cb) {

        client.clickOnMenu('#protocols', function() {
            client.waitForDataTableWasLoaded(function () {
                client.search(number, function () {
                    client.waitTableRowAndClickByText(number, function () {
                        client.clickOnEditButton(function () {
                            client.waitForSaveButton(cb);
                        })
                    })
                })
            })
        });
    };

    client.snapp.protocol.edit = function(number, data, cb) {
        data = data || {};

        client.snapp.protocol.openEditPage(number, function(){
            if (data.description) client.setValue('[name="description"]', data.description);
            if (data.number) client.setValue('[name="rx_type"]', data.number);
            if (data.name) client.setValue('[name="name"]', data.name);

            client.clickOnSaveButton(function () {
                client.waitForSuccessPopupAppeared(function(){
                    client.clickOnBackButton(function(){
                        client.waitForDataTableWasLoaded(cb)
                    })
                });
            });
        })
    };

    client.snapp.protocol.checkValues = function(number, data, cb) {
        data = data || {};

        client.snapp.protocol.openEditPage(number, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName('rx_type', function (text) {
                    assert.equal(text, number, 'Number was not saved.');
                    cb();
                });
            });
            if (data.name) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName('name', function (text) {
                        assert.equal(text, data.name, 'Name id was not saved.');
                        cb();
                    });
                });
            }
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName('description', function (text) {
                        assert.equal(text, data.description, 'Description id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){

                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.protocol.openManagePage = function(protocolNumber, cb){
        client.snapp.protocol.openEditPage(protocolNumber, function(){
            client.clickOnManageButton(function(){
                client.waitForDataTableWasLoaded(cb);
            })
        })
    };

    client.snapp.protocol.addProject = function(protocolNumber, projectOptions, cb){
        client.snapp.protocol.openManagePage(protocolNumber, function(){
            client.clickOnNewButton(function(){
                client.waitForPopupAppeared(function(){
                    client.snapp.project.fillAddProjectPopup(projectOptions, function () {
                        client.snapp.project.saveAddProjectPopup(function () {
                            client.snapp.project.waitForProjectEditPage(function () {
                                client.clickOnBackButton(function () {
                                    client.pause(1000);
                                    client.clickOnBackButton(function(){
                                        client.waitForDataTableWasLoaded(cb);
                                    });
                                })
                            })
                        });
                    });
                });
            })
        })
    };

    client.snapp.protocol.removeProject = function(protocolNumber, projectNumber, cb){
        client.snapp.protocol.openManagePage(protocolNumber, function(){
            client.snapp.common.searchAndDeleteTableRowWithText(projectNumber, function(){
                client.clickOnBackButton(function () {
                    client.waitForDataTableWasLoaded(cb);
                })
            });
        })
    };

    client.snapp.protocol.openManageTeamPage = function(protocolNumber, cb){
        client.snapp.protocol.openManagePage(protocolNumber, function(){
            client.clickOnMenu('#protocols/team/', function(){
                client.pause(1000); //wait until table is rendering
                client.waitForDataTableWasLoaded(cb);
            });
        })
    };

    client.snapp.protocol.openManageTemplatePage = function(protocolNumber, cb){
        client.snapp.protocol.openManagePage(protocolNumber, function(){
            client.clickOnMenu('#protocols/templates/', function(){
                client.pause(1000); //wait until table is rendering
                client.waitForDataTableWasLoaded(cb);
            });
        })
    };

    client.snapp.protocol.addAccountByCreation = function(protocolNumber, accountOptions, cb){
        client.snapp.protocol.openManageTeamPage(protocolNumber, function(){
            client.clickOnNewButton(function(){
                client.waitForPopupAppeared(function(){
                    client.snapp.account.fillAddAccountPopup(accountOptions, function () {
                        client.snapp.account.saveAddAccountPopup(function () {
                            client.waitForSuccessPopupAppeared(function(){
                                client.clickOnBackButton(function(){
                                    client.waitForDataTableWasLoaded(cb)
                                })
                            });
                        });
                    });
                });
            })
        })
    };

    client.snapp.protocol.removeAccount = function(protocolNumber, accountLastName, cb){
        client.snapp.protocol.openManageTeamPage(protocolNumber, function(){
            client.snapp.common.searchAndDeleteTableRowWithText(accountLastName, function(){
                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                })
            });
        })
    };

    client.snapp.protocol.checkIfAccountInTeam = function(protocolNumber, accountLastName, cb){
        client.snapp.protocol.openManageTeamPage(protocolNumber, function(){
            client.search(accountLastName, function(){
                client.isTableRowWithTextExist(accountLastName, cb);
            });
        })
    };

    client.snapp.protocol.makeSureIfAccountInTeam = function(protocolNumber, accountLastName, cb){
        client.snapp.protocol.checkIfAccountInTeam(protocolNumber, accountLastName, function(isInTeam){
            client.assert(isInTeam, 'Account does not in team.');

            client.clickOnBackButton(function(){
                client.waitForDataTableWasLoaded(cb)
            })
        })
    };

    client.snapp.protocol.addAccountBySelection = function(protocolNumber, accountLastName, cb){
        client.snapp.protocol.openManageTeamPage(protocolNumber, function(){
            client.clickOnSelectButton(function(){
                client.waitForSelectPopupAppeared(function(){
                    client.searchAndSelectRowInSelectPopup(accountLastName, function(){
                        client.submitSelectionInSelectPopup(function(){
                            client.waitForSelectPopupAppeared(function(){
                                client.waitForSuccessPopupAppeared(function(){
                                    client.clickOnBackButton(function(){
                                        client.waitForDataTableWasLoaded(cb)
                                    })
                                });
                            });
                        })
                    });
                });
            })
        })
    };

    client.snapp.protocol.addTemplate = function(protocolNumber, templateOptions, cb){
        client.snapp.protocol.openManageTemplatePage(protocolNumber, function(){
            client.clickOnNewButton(function(){
                client.waitForPopupAppeared(function(){
                    client.snapp.protocolTemplate.fillAddTemplatePopup(templateOptions, function () {
                        client.snapp.protocolTemplate.saveAddTemplatePopup(function () {
                            client.clickOnBackButton(function () {
                                client.pause(1000);
                                client.clickOnBackButton(function(){
                                    client.waitForDataTableWasLoaded(cb);
                                });
                            })
                        });
                    });
                });
            })
        })
    };

    client.snapp.protocol.checkIfTemplateAdded = function(protocolNumber, templateName, cb){
        client.snapp.protocol.openManageTemplatePage(protocolNumber, function(){
            client.search(templateName, function(){
                client.isTableRowWithTextExist(templateName, cb);
            });
        })
    };

    client.snapp.protocol.makeSureIfTemplateAdded = function(protocolNumber, templateName, cb){
        client.snapp.protocol.checkIfTemplateAdded(protocolNumber, templateName, function(isAdded){
            client.assert(isAdded, 'Template was not added.');

            client.isButtonVisible(client.snapp.buttons.newButton, function(isVisible){
                client.assert(!isVisible, 'New buttons is visible.');

                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb)
                })
            });
        })
    };

    client.snapp.protocol.removeTemplate = function(protocolNumber, templateName, cb){
        client.snapp.protocol.openManageTemplatePage(protocolNumber, function(){
            client.snapp.common.searchAndDeleteTableRowWithText(templateName, function(){
                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                })
            });
        })
    };

    client.snapp.protocol.tryToDelete = function(number, cb) {
        client.clickOnMenu('#protocols', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndTryToDeleteTableRowWithText(number, cb);
            });
        })
    };

    client.snapp.protocol.delete = function (number, cb) {
        client.clickOnMenu('#protocols', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndDeleteTableRowWithText(number, cb);
            });
        })
    };

    return client;
};
