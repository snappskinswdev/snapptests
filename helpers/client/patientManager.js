/**
 * Created by p.hudinsky on 5/20/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper("unique");

function getSearchSelector(patientId) {
    return '//td[text()="' + patientId + '"]';
}

module.exports.extend = function (client) {
    client.snapp.patient = {};

    client.snapp.patient.formElementNames = {
        patientId: 'patient_id',
        otherId: 'other_id',
        firstName: 'first_name',
        lastName: 'last_name',
        birthday: 'birthday',
        ssn: 'ssn'
    };
    var formElementNames = client.snapp.patient.formElementNames;

    client.snapp.patient.fillAddPopup = client.snapp.patient.fillEditForm = function(data, cb){
        data.patientId = data.patientId || unique.generate('PatientId_');
        data.firstName = data.firstName || unique.generate('FirstName_');
        data.lastName = data.lastName || unique.generate('LastName_');
        data.birthday = data.birthday || unique.generateDate();

        if (data.patientId) client.setTextInputValueByName(formElementNames.patientId, data.patientId);
        if (data.firstName) client.setTextInputValueByName(formElementNames.firstName, data.firstName);
        if (data.lastName) client.setTextInputValueByName(formElementNames.lastName, data.lastName);
        if (data.birthday) client.setTextInputValueByName(formElementNames.birthday, data.birthday);
        if (data.otherId) client.setTextInputValueByName(formElementNames.otherId, data.otherId);
        if (data.ssn) client.setTextInputValueByName(formElementNames.ssn, data.ssn);

        cb();
    };

    client.snapp.patient.saveAddPopup = function(cb){
        client.savePopup(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.waitForDataTableWasLoaded(cb)
            });
        });
    };

    client.snapp.patient.saveEditForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.clickOnBackButton(function() {
                    client.waitForDataTableWasLoaded(cb)
                });
            });
        });
    };

    client.snapp.patient.create = function(patientOptions, cb) {
        patientOptions = patientOptions || {};
        if (!patientOptions.ssn && !patientOptions.otherId) {
            throw 'Ssn or otherId must be provided.';
        }

        patientOptions.patientId = patientOptions.patientId || unique.generate('PatientId_');
        patientOptions.firstName = patientOptions.firstName || unique.generate('FirstName_');
        patientOptions.lastName = patientOptions.lastName || unique.generate('LastName_');
        patientOptions.birthday = patientOptions.birthday || unique.generateDate();

        client.clickOnMenu('#patients', function() {

            client.clickOnNewButton(function(){
                client.waitForPopupAppeared(function(){
                    client.snapp.patient.fillAddPopup(patientOptions, function(){
                        client.snapp.patient.saveAddPopup(cb);
                    });
                });
            })
        });
    };

    client.snapp.patient.openEditPage = function(patientId, cb) {
        var searchProjectString = getSearchSelector(patientId);

        client.clickOnMenu('#patients', function() {
            client.waitForDataTableWasLoaded(3000, function () {

                client.search(patientId, function () {

                    client.waitAndClick(searchProjectString, 3000, function (res) {
                        client.assert(res, "Patient '" + patientId + "' was not found or search functionality does not work.");

                        client.clickOnEditButton(function () {

                            client.waitForSaveButton(function () {
                                client.pause(2000); //wait untill data was loaded;

                                cb();
                            });
                        })
                    })
                })
            })
        });
    };

    client.snapp.patient.edit = function(patientId, patientOptions, cb) {
        patientOptions = patientOptions || {};

        client.snapp.patient.openEditPage(patientId, function(){
            client.snapp.patient.fillEditForm(patientOptions, function(){
                client.snapp.patient.saveEditForm(cb);
            });
        })
    };

    client.snapp.patient.checkValues = function(patientId, patientOptions, cb) {
        patientOptions = patientOptions || {};

        client.snapp.patient.openEditPage(patientId, function(){
            var callbacks = [];

            if (patientOptions.patientId) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.patientId, function (text) {
                        assert.equal(text, patientOptions.patientId, 'Patient\'s id was not saved.');
                        cb();
                    });
                });
            }
            if (patientOptions.firstName) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.firstName, function (text) {
                        assert.equal(text, patientOptions.firstName, 'Patient\'s first name was not saved.');
                        cb();
                    });
                });
            }
            if (patientOptions.lastName) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.lastName, function (text) {
                        assert.equal(text, patientOptions.lastName, 'Patient\'s last name was not saved.');
                        cb();
                    });
                });
            }
            if (patientOptions.birthday) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.birthday, function (text) {
                        assert.equal(text, patientOptions.birthday, 'Patient\'s birthday was not saved.');
                        cb();
                    });
                });
            }
            if (patientOptions.otherId) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.otherId, function (text) {
                        assert.equal(text, patientOptions.otherId, 'Patient\'s other id was not saved.');
                        cb();
                    });
                });
            }
            if (patientOptions.ssn) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(formElementNames.ssn, function (text) {
                        var assertions = (text == patientOptions.ssn) || (text.replace(/-/g, "") == patientOptions.ssn.replace(/-/g, ""));
                        client.assert(assertions, 'Patient\'s ssn was not saved.');
                        cb();
                    });
                });
            }

            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){

                    client.waitForDataTableWasLoaded(6000, function(){
                        cb();
                    });
                });
            });
        });
    };

    client.snapp.patient.tryToDelete = function(patientId, cb) {
        client.clickOnMenu('#patients', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndTryToDeleteTableRowWithText(patientId, cb);
            });
        })
    };

    client.snapp.patient.delete = function (patientId, cb) {
        client.clickOnMenu('#patients', function () {
            client.waitForDataTableWasLoaded(function () {
                client.snapp.common.searchAndDeleteTableRowWithText(patientId, cb);
            });
        })
    };

    return client;
};
