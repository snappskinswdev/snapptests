/**
 * Created by p.hudinsky on 9/24/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var
    manufacturerInputName = 'manufacturer',
    modelInputName = 'model',
    descriptionInputName = 'description'
    ;

module.exports.extend = function (client) {
    client.snapp.snaType = {};

    client.snapp.snaType.generateTypeString = function(manufacturer, model){
        var typeString = '';
        if (arguments.length === 1 && typeof manufacturer === 'object') {
            var typeData = manufacturer;
            typeString = typeData.manufacturer + ' ' + typeData.model;
        } else {
            typeString = manufacturer + ' ' + model;
        }
        return typeString;
    };

    client.snapp.snaType.fillAddSensorTypeForm = function(data, cb){
        data.manufacturer = data.manufacturer || unique.generate('String_');
        data.description = data.description || unique.generate('String_');
        data.model = data.model || unique.generate('String_');

        if (data.manufacturer) client.setTextInputValueByName(manufacturerInputName, data.manufacturer);
        if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);
        if (data.model) client.setTextInputValueByName(modelInputName, data.model);
        cb();
    };

    client.snapp.snaType.saveAddSnaTypeForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.waitForDataTableWasLoaded(cb)
            });
        });
    };

    client.snapp.snaType.create = function (data, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas/types"]', function () {
                client.waitForNewButton(function () {
                    client.clickOnNewButton(function () {
                        client.waitForSaveButton(function () {
                            client.snapp.snaType.fillAddSensorTypeForm(data, function () {
                                client.snapp.snaType.saveAddSnaTypeForm(cb);
                            });
                        });
                    })
                })
            })
        })
    };

    client.snapp.snaType.openEditPage = function(model, cb) {

        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.search(model, function () {
                        client.waitTableRowAndClickByText(model, function () {
                            client.clickOnEditButton(function () {
                                client.waitForSaveButton(cb);
                            })
                        })
                    })
                })
            })
        });
    };

    client.snapp.snaType.edit = function(model, data, cb) {
        data = data || {};

        client.snapp.snaType.openEditPage(model, function(){
            client.snapp.snaType.fillAddSensorTypeForm(data, function(){
                client.snapp.snaType.saveAddSnaTypeForm(cb);
            });
        })
    };

    client.snapp.snaType.checkValues = function(model, data, cb) {
        data = data || {};

        client.snapp.snaType.openEditPage(model, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(modelInputName, function (text) {
                    assert.equal(text, model, 'model was not saved.');
                    cb();
                });
            });
            if (data.manufacturer) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(manufacturerInputName, function (text) {
                        assert.equal(text, data.manufacturer, 'manufacturer id was not saved.');
                        cb();
                    });
                });
            }
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(descriptionInputName, function (text) {
                        assert.equal(text, data.description, 'description id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.snaType.tryToDelete = function(model, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndTryToDeleteTableRowWithText(model, cb);
                });
            })
        })
    };

    client.snapp.snaType.delete = function (model, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#snas/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndDeleteTableRowWithText(model, cb);
                });
            })
        })
    };

    return client;
};
