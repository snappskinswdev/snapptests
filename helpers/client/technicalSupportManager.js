/**
 * Created by p.hudinsky on 9/24/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

module.exports.extend = function (client) {
    client.snapp.technicalSupport = {};

    client.snapp.technicalSupport.openPage = function (cb) {
        client.clickOnMenu("#technical-support", function () {
            client.waitForDataTableWasLoaded(cb);
        });
    };

    client.snapp.technicalSupport.addAccount = function (accountName, cb) {
        client.snapp.technicalSupport.openPage(function () {
            client.clickOnNewButton(function () {
                client.waitForSelectPopupAppeared(function () {
                    client.searchAndSelectRowInSelectPopup(accountName, function () {
                        client.submitSelectionInSelectPopup(function(){
                            client.waitForSuccessPopupAppeared(function(){
                                client.waitForDataTableWasLoaded(cb)
                            });
                        })
                    })
                })
            })
        });
    };

    client.snapp.technicalSupport.tryToRemoveAccount = function (accountName, cb) {
        client.snapp.technicalSupport.openPage(function () {
            client.snapp.common.searchAndTryToDeleteTableRowWithText(accountName, cb);
        });
    };

    client.snapp.technicalSupport.removeAccount = function (accountName, cb) {
        client.snapp.technicalSupport.openPage(function () {
            client.snapp.common.searchAndDeleteTableRowWithText(accountName, cb);
        })
    };

    return client;
};
