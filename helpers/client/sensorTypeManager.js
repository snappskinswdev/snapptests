/**
 * Created by p.hudinsky on 9/24/2015.
 */
var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var
    manufacturerInputName = 'manufacturer',
    modelInputName = 'model',
    descriptionInputName = 'description',
    fetchRateInputName = 'fetchRate'
    ;

module.exports.extend = function (client) {
    client.snapp.sensorType = {};

    client.snapp.sensorType.fillAddSensorTypeForm = function(data, cb){
        data.manufacturer = data.manufacturer || unique.generate('String_');
        data.description = data.description || unique.generate('String_');
        data.model = data.model || unique.generate('String_');
        data.fetchRate = data.fetchRate || unique.generateNumbers(2);

        if (data.manufacturer) client.setTextInputValueByName(manufacturerInputName, data.manufacturer);
        if (data.description) client.setTextInputValueByName(descriptionInputName, data.description);
        if (data.fetchRate) client.setTextInputValueByName(fetchRateInputName, data.fetchRate);
        if (data.model) client.setTextInputValueByName(modelInputName, data.model);
        cb();
    };

    client.snapp.sensorType.saveAddSensorTypeForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.waitForDataTableWasLoaded(cb)
            });
        });
    };

    client.snapp.sensorType.saveEditSensorTypeForm = function(cb){
        client.clickOnSaveButton(function(){
            client.waitForSuccessPopupAppeared(function(){
                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.sensorType.create = function (data, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#sensors/types"]', function () {
                client.waitForNewButton(function () {
                    client.clickOnNewButton(function () {
                        client.waitForSaveButton(function () {
                            client.snapp.sensorType.fillAddSensorTypeForm(data, function () {
                                client.snapp.sensorType.saveAddSensorTypeForm(cb);
                            });
                        });
                    })
                })
            })
        })
    };

    client.snapp.sensorType.openEditPage = function(model, cb) {

        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#sensors/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.search(model, function () {
                        client.waitTableRowAndClickByText(model, function () {
                            client.clickOnEditButton(function () {
                                client.waitForSaveButton(cb);
                            })
                        })
                    })
                })
            })
        });
    };

    client.snapp.sensorType.edit = function(model, data, cb) {
        data = data || {};

        client.snapp.sensorType.openEditPage(model, function(){
            client.snapp.sensorType.fillAddSensorTypeForm(data, function(){
                client.snapp.sensorType.saveEditSensorTypeForm(cb);
            });
        })
    };

    client.snapp.sensorType.checkValues = function(model, data, cb) {
        data = data || {};

        client.snapp.sensorType.openEditPage(model, function(){
            var callbacks = [];

            callbacks.push(function (cb) {
                client.getTextInputValueByName(modelInputName, function (text) {
                    assert.equal(text, model, 'model was not saved.');
                    cb();
                });
            });
            if (data.manufacturer) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(manufacturerInputName, function (text) {
                        assert.equal(text, data.manufacturer, 'manufacturer id was not saved.');
                        cb();
                    });
                });
            }
            if (data.description) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(descriptionInputName, function (text) {
                        assert.equal(text, data.description, 'description id was not saved.');
                        cb();
                    });
                });
            }
            if (data.fetchRate) {
                callbacks.push(function (cb) {
                    client.getTextInputValueByName(fetchRateInputName, function (text) {
                        assert.equal(text, data.fetchRate, 'fetchRate id was not saved.');
                        cb();
                    });
                });
            }
            async.series(callbacks, function(err){
                if (err) throw err;

                client.clickOnBackButton(function(){
                    client.waitForDataTableWasLoaded(cb);
                });
            });
        });
    };

    client.snapp.sensorType.tryToDelete = function(model, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#sensors/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndTryToDeleteTableRowWithText(model, cb);
                });
            })
        })
    };

    client.snapp.sensorType.delete = function (model, cb) {
        client.clickOnMenu('#snas/types', function () {
            client.clickOnMenu('a[href="#sensors/types"]', function () {
                client.waitForDataTableWasLoaded(function () {
                    client.snapp.common.searchAndDeleteTableRowWithText(model, cb);
                });
            })
        })
    };

    return client;
};
