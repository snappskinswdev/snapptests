var assert = require("assert");
var unique = require("./unique");

function isFunction(obj) {
    return !!(obj && obj.constructor && obj.call && obj.apply);
};

module.exports.extend = function (client) {

    client.snapp = {}; // namespace
    client.snapp.buttons = {
        newButton: {
            selector: '#new-button'
        },
        checkin: {
            selector: '#checkin'
        },
        checkout: {
            selector: '#checkout-button'
        },
        changeLocation: {
            selector: '#change-location'
        },
        upload: {
            selector: '#upload'
        }
    };

    function getButtonSelector(buttonData) {
        var selector = buttonData;
        if (typeof buttonData == 'object') {
            selector = buttonData.selector;
        }

        return selector;
    }

    function saveScreenshot(name, cb) {
        var screenshotPath = global.config.SCREENSHOT_FOLDER + '/' + unique.generate('_') + '.png';
        client.saveScreenshot(screenshotPath, function(err, screenshot, res){
            if (err) throw err;
            cb(screenshotPath);
        });

        return screenshotPath;
    }

    function throwIfError(err) {

        if (err) {
            saveScreenshot('Error', function (path) {
                throw err;
            });
        }
    }

    client.throwIfError = throwIfError;

    client.assert = function (assertion, message) {
        message = message || 'Error';
        if (!assertion) {
            saveScreenshot(message, function (path) {
                message += ' See screenshot here: ' + path;
                assert(assertion, message);
            });
        }
    };

    client.clickOnMenu = function (menuSelector, callback) {

        var currentClient = this;
        currentClient.isVisible(menuSelector, function (err, isVisible) {
            if (isVisible) {
                currentClient.click(menuSelector, function (err) {
                    throwIfError(err);
                    callback();
                });
            } else {
                currentClient.click('#general', function (err) {
                    throwIfError(err);
                    currentClient.waitForVisible(menuSelector, 3000, false, function (err, res) {
                        client.assert(res);
                        currentClient.click(menuSelector, function (err) {
                            throwIfError(err);
                            callback();
                        });
                    })
                })
            }
        });
    };

    client.isButtonVisible = function (button, cb) {
        var buttonSelector = getButtonSelector(button);
        client.isVisible(buttonSelector, function (err, isVisible) {
            throwIfError(err);
            cb(isVisible);
        });
    };

    client.clickOnRegistrationMenu = function (menuSelector, callback) {

        client.click('#registration', function (err) {
            throwIfError(err);

            client.waitForVisible(menuSelector, 3000, false, function (err, res) {
                client.assert(res, "Button was not displayed.");

                client.click(menuSelector, function (err) {
                    throwIfError(err);
                    callback();
                });
            })
        })
    };

    client.waitForSuccessPopup = function (time, revert, callback) {
        var currentClient = this;
        currentClient.waitForExist('#alertify-logs article.alertify-log-success', time, revert, function (err, res) {
            callback(err, res);
        });
    };

    client.waitForSuccessPopupAppeared = function (time, cb) {
        if (isFunction(time)) {
            cb = time;
            time = 6000;
        }
        client.waitForSuccessPopup(6000, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Popup was not appeared");

            cb();
        })
    };

    client.waitForErrorPopup = function (time, revert, callback) {
        var currentClient = this;
        currentClient.waitForExist('#alertify-logs article.alertify-log-error', time, revert, function (err, res) {
            callback(err, res);
        });
    };

    client.waitForErrorPopupAppeared = function (cb) {
        client.waitForErrorPopup(6000, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Popup was not appeared");

            cb();
        })
    };

    client.waitForConfirmationPopup = function (time, revert, callback) {
        var currentClient = this;

        currentClient.waitForExist('#confirmation', time, revert, function (err, res) {

            if (res) {
                callback(err, res);
            } else {
                client.isVisible('#confirmation', function (err, isVisible) {
                    res = res || ((isVisible && !revert) || (!isVisible && revert));
                    callback(err, res);
                });
            }
        });
    };
    client.waitForConfirmationPopupAppeared = function (cb) {
        client.waitForConfirmationPopup(9000, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Confirmation popup was not appeared.");
            cb();
        });
    };

    client.confirmPopup = function (cb) {
        var confirmButtonSelector = '#confirmation';
        client.waitForExists(confirmButtonSelector, 1000, function () {
            client.click(confirmButtonSelector, function (err) {
                throwIfError(err);
                cb();
            });
        });
    };

    client.waitForSelectPopup = function (time, revert, callback) {
        var currentClient = this;

        currentClient.waitForExist('#select-popup-button', time, revert, function (err, res) {

            client.pause(2000); //select popup does not have enough time to refresh list after previous using and wee need to wait

            if (res) {
                callback(err, res);
            } else {
                client.isVisible('#select-popup-button', function (err, isVisible) {
                    if (isVisible) {
                        if (!revert) {
                            callback(err, true);
                        } else {
                            client.waitForVisible('#select-popup-button', time, true, function (err, res) {
                                callback(err, res);
                            });
                        }
                    } else {
                        if (!revert) {
                            client.waitForVisible('#select-popup-button', time, false, function (err, res) {
                                callback(err, res);
                            });
                        } else {
                            callback(err, true);
                        }
                    }
                });
            }
        });
    };

    client.waitForSelectPopupAppeared = function (cb) {
        client.waitForSelectPopup(9000, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Select popup was not appeared.");
            cb();
        })
    };

    client.submitSelectionInSelectPopup = function (cb) {
        client.clickOnSelectPopupButton(cb);
    };

    client.searchAndSelectRowInSelectPopup = function (text, cb) {
        client.searchInSelectPopup(text, function () {
            client.waitTableRowAndClickByText(text, cb);
        });
    };

    client.waitForPopup = function (time, revert, cb) {
        client.waitForExist('#save-popup-button', time, revert, function (err, res) {
            cb(err, res);
        });
    };

    client.waitForPopupAppeared = function (cb) {
        client.waitForPopup(9000, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Popup was not appeared.");
            cb();
        })
    };

    client.cancelPopup = function (cb) {
        client.click("#cancel-popup-button", function (err) {
            throwIfError(err);
            client.pause(2000);
            cb();
        })
    };

    client.savePopup = function (cb) {
        client.click("#save-popup-button", function (err) {
            throwIfError(err);
            client.pause(2000);
            cb();
        })
    };

    client.waitForExists = function (selector, time, callback) {
        client.waitForExist(selector, time, false, function (err, res) {
            throwIfError(err);
            client.assert(res, "Element with selector '" + selector + "' does not exist.");
            callback(res);
        });
    };

    /**
     * @param callback
     * @param throwIfNotAppeared (optional)
     */
    client.waitForSaveButton = function (callback, throwIfNotAppeared) {
        throwIfNotAppeared = throwIfNotAppeared || true;

        client.waitForExists('#save-button', 5000, function (res) {
            if (throwIfNotAppeared) {
                client.assert(res, 'Save buttons was not appeared.');
            }
            callback(res);
        });
    };

    client.waitForNotExists = function (selector, time, callback) {
        client.waitForExist(selector, time, true, function (err, res) {
            throwIfError(err);
            callback(res);
        });
    };

    client.search = function (subject, callback) {
        var searchInputSelector = '#search input[type="text"]';
        var searchInputSelector2 = '.table-container:not([style*="display: none"]) .search input[type="text"]';

        client.isExisting(searchInputSelector, function (err, isExists) {
            throwIfError(err);

            if (isExists) {
                client.setValue(searchInputSelector, subject, function () {
                    client.pause(2000);
                    callback();
                });
            } else {
                client.isExisting(searchInputSelector2, function (err, isExists) {
                    throwIfError(err);

                    if (isExists) {
                        client.setValue(searchInputSelector2, subject, function () {
                            client.pause(2000);
                            callback();
                        });
                    } else {
                        throw "Search element is not exists in DOM.";
                    }
                });
            }
        });
    };

    client.isTableRowWithTextExist = function (textToSearch, cb) {
        var selector = '//td[text()="' + textToSearch + '"]';

        client.isExisting(selector, function (err, isExists) {
            throwIfError(err);

            cb(isExists);
        });
    };

    client.searchInSelectPopup = function (subject, callback) {
        var searchInputSelector = '#select-search input[type="text"]';

        client.isExisting(searchInputSelector, function (err, isExists) {
            throwIfError(err);

            if (isExists) {
                client.setValue(searchInputSelector, subject, function () {
                    client.pause(2000);
                    callback();
                });
            } else {

                throw "Search element in select popup is not exists in DOM.";
            }
        });

    };

    client.waitTableRowAndClickByText = function (textToSearch, callback) {
        var selector = '//td[text()="' + textToSearch + '"]';
        client.waitAndClick(selector, 6000, function (res) {
            client.assert(res, 'Row with text "' + textToSearch + '" was not found.');
            callback();
        });
    };

    client.waitAndClick = function (selector, time, callback) {

        client.waitForExists(selector, time, function (res) {
            if (!res) {
                callback(res);
                return;
            }
            client.click(selector, function (err) {
                throwIfError(err);

                callback(res);
            })
        });
    };

    client.waitForDataTableWasLoaded = function (time, cb) {
        if (isFunction(time)) {
            cb = time;
            time = 2000;
        }
        client.waitForExist("#data-container table.p-81", time, false, function (err, res) {
            throwIfError(err);
            if (res) {
                cb();
            } else {
                client.waitForExists("div.table-container table", time, function (res) {
                    client.assert(res, "Data table was not loaded.");
                    cb();
                });
            }
        });
    };

    client.waitForPopupErrorText = function (time, revert, cb) {
        client.waitForText('#error-popup-placeholder', time, revert, function (err, res) {
            cb(err, res);
        })
    };

    client.waitForNewButton = function (cb) {
        client.waitForExists("#new-button", 9000, cb);
    };

    client.clickOnNewButton = function (cb) {
        client.click("#new-button", function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnDeleteButton = function (cb) {
        client.click("#delete-button", cb);
    };

    client.clickOnViewButton = function (cb) {
        client.click("#view-button", cb);
    };

    client.clickOnEditButton = function (cb) {
        client.click("#edit-button", cb);
    };

    client.clickOnSaveButton = function (cb) {
        client.click("#save-button", cb);
    };

    client.clickOnBackButton = function (cb) {
        client.click("#back-button", cb);
    };

    client.clickOnManageButton = function (cb) {
        client.click("#manage-button", function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnSelectPopupButton = function (cb) {
        client.click("#select-popup-button", function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnSelectButton = function (cb) {
        client.click("#select-button", function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnCheckinButton = function (cb) {
        client.click(client.snapp.buttons.checkin.selector, function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnChangeLocationButton = function (cb) {
        client.click(client.snapp.buttons.changeLocation.selector, function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnCheckoutButton = function (cb) {
        client.click(client.snapp.buttons.checkout.selector, function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnUploadButton = function (cb) {
        client.click(client.snapp.buttons.upload.selector, function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnShowArchivedButton = function (cb) {
        client.click('#show-archived label[for="show-archived-input"]', function (err) {
            throwIfError(err);
            cb();
        });
    };

    client.clickOnSavePopupButton = function (cb) {
        client.savePopup(cb);
    };

    client.getTextInputValueByName = function (name, cb) {
        client.getValue('[name="' + name + '"]', function (err, text) {
            throwIfError(err);
            cb(text);
        });
    };

    client.setTextInputValueByName = function (name, value, cb) {
        cb = cb || function () {
            };
        client.setValue('[name="' + name + '"]', value);
        cb();
    };

    client.chooseFileByInputName = function (name, file, cb) {
        cb = cb || function () {};
        client.chooseFile('[name="' + name + '"]', file);
        cb();
    };

    client.select = function (selectName, text, cb) {
        cb = cb || function () {
            };
        client.selectByVisibleText('[name="' + selectName + '"]', text);
        cb();
    };

    client.isFormElementWithNameVisible = function (name, cb) {
        var selector = '[name="' + name + '"]';

        client.isExisting(selector, function (err, isExists) {
            throwIfError(err);

            if (!isExists) {
                cb(false);
            } else {
                client.isVisible(selector, function (err, isVisible) {
                    throwIfError(err);
                    cb(isVisible);
                });
            }
        });

    };

    client.snapp.common = {};
    client.snapp.common.searchAndTryToDeleteTableRowWithText = function (text, cb) {
        var searchRow = '//td[text()="' + text + '"]';

        client.search(text, function () {
            client.waitAndClick(searchRow, 3000, function (res) {
                client.assert(res, "Row with text '" + text + "' was not found or search functionality does not work.");

                client.clickOnDeleteButton(function () {
                    client.waitForConfirmationPopupAppeared(function () {
                        client.confirmPopup(function () {
                            cb();
                        });
                    })
                })
            });
        });
    };
    client.snapp.common.searchAndDeleteTableRowWithText = function (text, cb) {
        var searchRow = '//td[text()="' + text + '"]';

        client.snapp.common.searchAndTryToDeleteTableRowWithText(text, function () {
            client.waitForSuccessPopupAppeared(function () {
                client.waitForNotExists(searchRow, 9000, function (res) {
                    client.assert(res, 'Row with text "' + text + '" was not deleted.');
                    cb();
                });
            })
        });
    };
    client.snapp.common.searchAndCheckIsTableRowWithTextExist = function (text, cb) {
        client.search(text, function () {
            client.isTableRowWithTextExist(text, cb);
        });
    };

    client = require('./client/snaTypeManager').extend(client);
    client = require('./client/snaManager').extend(client);
    client = require('./client/protocolManager').extend(client);
    client = require('./client/projectManager').extend(client);
    client = require('./client/patientManager').extend(client);
    client = require('./client/accountManager').extend(client);
    client = require('./client/protocolTemplateManager').extend(client);
    client = require('./client/roleManager').extend(client);
    client = require('./client/sensorTypeManager').extend(client);
    client = require('./client/technicalSupportManager').extend(client);
    client = require('./client/assetManager').extend(client);

    return client;
};
