var shortid = require('shortid');

module.exports.generateNumbers = generateNumbers = function(numbersCount) {
    numbersCount = numbersCount || 10;
    var setOfNumbers = [0,1,2,3,4,5,6,7,8,9];

    var random = '';
    while (numbersCount) {
        var number = setOfNumbers[Math.floor(Math.random() * setOfNumbers.length)];
        if (!random && !number) { //prevent first number is 0
            continue;
        }
        random += number;
        numbersCount--;
    }
    for (var i = 0; i < numbersCount; i++) random += setOfNumbers[Math.floor(Math.random() * setOfNumbers.length)];
    return random;
};

module.exports.generate = generate = function(prefix){
    prefix = prefix || '';
    return prefix + shortid.generate();
};

module.exports.generateEmail = function(){
    var email = generate('') + generate('@') + '.com';

    return email.replace(/\s|_/g, '');
};

module.exports.generatePhone = function(numbersCount){
    numbersCount = numbersCount || 10;
    return generateNumbers(numbersCount);
};

module.exports.generateSsn = function(numbersCount){
    numbersCount = numbersCount || global.config.client.patient.ssnSize;
    return generateNumbers(numbersCount);
};

module.exports.generateDate = function(){
    var year = 1814 + Math.floor(Math.random() * 200); // from 1814 to 2014
    var month = 1 + Math.floor(Math.random() * 11);
    month = month.toString();
    if (month.length == 1) {
        month = '0' + month;
    }
    var day = 1 + Math.floor(Math.random() * 27); // from 1 to 28 for prevent february collaps
    day = day.toString();
    if (day.length == 1) {
        day = '0' + day;
    }
    return month + '/' + day + '/' + year;
};