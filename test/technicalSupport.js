var assert = require("assert");
var unique = global.loadHelper('unique');
var client = global.client;

var accountData = {
    firstName: unique.generate('FirstName_'),
    lastName: unique.generate('LastName_')
};

describe('Technical support', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    before('Create environment', function (done) {
        client.snapp.account.create(accountData, done);
    });

    it('Account must be added to technical support group', function (done){
        client.snapp.technicalSupport.addAccount(accountData.lastName, done);
    });

    it('Added account must be checked', function (done){
        client.snapp.technicalSupport.openPage(function () {
            client.snapp.common.searchAndCheckIsTableRowWithTextExist(accountData.lastName, function (isExist) {
                client.assert(isExist, 'Account is not added to tech support');
                done();
            })
        });
    });

    it('Account must be removed from technical support group', function (done){
        client.snapp.technicalSupport.removeAccount(accountData.lastName, done);
    });


    it('Added account must be checked', function (done){
        client.snapp.technicalSupport.openPage(function () {
            client.snapp.common.searchAndCheckIsTableRowWithTextExist(accountData.lastName, function (isExist) {
                client.assert(!isExist, 'Account is not removed from tech support');
                done();
            })
        });
    });

    after('Remove environment', function (done) {
        client.snapp.account.delete(accountData.lastName, done);
    });
});
