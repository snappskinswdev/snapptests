var assert = require("assert");
var client = global.client;
var unique = global.loadHelper('unique');

var protocolOptions = {
    number: unique.generate('Number_')
};
var protocolOptions2 = {
    number: unique.generate('Number_'),
    name: unique.generate('Name_'),
    description: unique.generate('Description_')
};

var projectOptions = {
    rxNumber: unique.generate('RxNumber_'),
    protocolString: client.snapp.protocol.generateProtocolSelectString(protocolOptions2)
};

var accountPassword = unique.generate('SnappSkin&1_');
var accountOptions = {
    firstName: unique.generate('FirstName_'),
    lastName: unique.generate('LastName_'),
    password: accountPassword,
    passwordConfirm: accountPassword,
    username: unique.generate('Username_'),
    email: unique.generateEmail(),
    phone: unique.generatePhone()
};

var templateOptions = {
    name: unique.generate('Name_'),
    description: unique.generate('Description_')
};

describe('Protocols', function () {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('New protocol must be created.', function (done) {
        client.snapp.protocol.create(protocolOptions, done);
    });

    it('Protocol must be checked.', function (done) {
        client.snapp.protocol.checkValues(protocolOptions.number, protocolOptions, done);
    });

    it('Protocol must be updated.', function (done) {
        client.snapp.protocol.edit(protocolOptions.number, protocolOptions2, done);
    });

    it('Protocol must be checked.', function (done) {
        client.snapp.protocol.checkValues(protocolOptions2.number, protocolOptions2, done);
    });

    it('Manage protocol add new project', function (done) {
        client.snapp.protocol.addProject(protocolOptions2.number, projectOptions, done)
    });

    it('Protocol must not be deleted.', function (done) {
        var searchProtocolString = '//td[text()="' + protocolOptions2.number + '"]';

        client.snapp.protocol.tryToDelete(protocolOptions2.number, function(){
            client.waitForErrorPopupAppeared(function () {
                client.waitForExists(searchProtocolString, 3000, function (res) {
                    client.assert(res, 'Protocol was not deleted.');

                    done();
                });
            })
        });
    });

    it('Project of protocol must be deleted', function (done) {
        client.snapp.protocol.removeProject(protocolOptions2.number, projectOptions.rxNumber, done);
    });

    it('New member must be added via creation', function (done) {
        client.snapp.protocol.addAccountByCreation(protocolOptions2.number, accountOptions, done);
    });

    it('New account must be in team', function (done) {
        client.snapp.protocol.makeSureIfAccountInTeam(protocolOptions2.number, accountOptions.lastName, done);
    });

    it('Account must be removed from team', function (done) {
        client.snapp.protocol.removeAccount(protocolOptions2.number, accountOptions.lastName, done);
    });

    it('New member must be added via selection', function (done) {
        client.snapp.protocol.addAccountBySelection(protocolOptions2.number, accountOptions.lastName, done);
    });

    it('Protocol template must be added.', function (done) {
        client.snapp.protocol.addTemplate(protocolOptions2.number, templateOptions, done);
    });

    it('Protocol template must be checked.', function (done) {
        client.snapp.protocol.makeSureIfTemplateAdded(protocolOptions2.number, templateOptions.name, done);
    });

    it('Protocol template must be deleted.', function (done) {
        client.snapp.protocol.removeTemplate(protocolOptions2.number, templateOptions.name, done);
    });

    it('Protocol must be deleted.', function (done) {
        client.snapp.protocol.delete(protocolOptions2.number, done);
    });


    after('Remove created account', function (done) {
        client.snapp.account.delete(accountOptions.lastName, done);
    });
});