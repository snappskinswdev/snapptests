var assert = require("assert");
var unique = global.loadHelper("unique");
var client = global.client;

var patientOptions = {
    ssn: unique.generateSsn(),
    patientId: unique.generate('PatientId_')
};

var patientOptions2 = {
    patientId: unique.generate('PatientId2_')
};

describe('Patients', function() {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('New patient must be created', function (done) {
        client.snapp.patient.create(patientOptions, done);
    });

    it('New values must be checked', function (done) {
        client.snapp.patient.checkValues(patientOptions.patientId, patientOptions, done);
    });

    it('Patient must be updated', function (done) {
        client.snapp.patient.edit(patientOptions.patientId, patientOptions2, done);
    });

    it('New values must be checked', function (done) {
        client.snapp.patient.checkValues(patientOptions2.patientId, patientOptions2, done);
    });

    it('New patient must be deleted', function (done) {
        client.snapp.patient.delete(patientOptions2.patientId, done);
    });
});





