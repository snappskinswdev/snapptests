after(function (done) {
    this.timeout(global.config.SUIT_TIMEOUT);

    global.client.end(function () {
        global.server.shutdown().then(function () {
            done()
        });
    });
});