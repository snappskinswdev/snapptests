var assert = require("assert");
var unique = global.loadHelper('unique');
var client = global.client;

var snaConfigFileName = '324kl23423l423k42.zip';
var snaConfigData = {
    description: unique.generate('String_'),
    file: './test/assets/' + snaConfigFileName,
    fileName: snaConfigFileName
};

var snaSoftFileName = 'MiFit_1.4.152_1025.apk';
var snaSoftData = {
    description: unique.generate('String_'),
    releasedDate: unique.generateDate(),
    file: './test/assets/' + snaSoftFileName,
    fileName: snaSoftFileName
};

describe('Assets', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    before('Create environment', function (done) {
        client.snapp.asset.checkIfSnaSoftUploaded(snaSoftData.fileName, function (isExists) {
            if (isExists) {
                client.snapp.asset.deleteSnaSoft(snaSoftData.fileName, done);
            } else {
                done();
            }
        });
    });

    it('Sna soft must be uploaded', function (done) {
        client.snapp.asset.uploadSnaSoft(snaSoftData, done);
    });

    it('Sna soft must be checked for unique error', function (done) {
        client.snapp.asset.tryToUploadSnaSoft(snaSoftData, function(){
            client.waitForText('#error-popup-placeholder', 20000, false, function(err, res){
                client.assert(res, "Error message was not appeared. Maybe file is upload successfully");

                client.getText('#error-popup-placeholder', function(err, text) {
                    client.throwIfError(err);

                    var isExpected = text.search('already exists') != -1;
                    if (!isExpected) {
                        console.warn('Original error message: "%s"', text);
                    }
                    client.assert(isExpected, 'Error message is unexpected. Expected message with "already exists" words');

                    client.cancelPopup(function(){
                        client.search('test string', function(){
                            client.waitForDataTableWasLoaded(done);
                        });
                    })
                })
            })
        });
    });

    it('Sna soft must be deleted', function (done) {
        client.snapp.asset.deleteSnaSoft(snaSoftData.fileName, done);
    });

    it('Sna config must be uploaded', function (done) {
        client.snapp.asset.uploadSnaConfig(snaConfigData, done);
    });

    it('Sna config must be deleted', function (done) {
        client.snapp.asset.deleteSnaConfig(snaConfigData.description, done);
    });
});
