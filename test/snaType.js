var assert = require("assert");
var unique = global.loadHelper('unique');
var client = global.client;

var data = {
    model: unique.generate('String_'),
    manufacturer: unique.generate('String_'),
    description: unique.generate('String_')
};
var data2 = {
    model: unique.generate('String_'),
    manufacturer: unique.generate('String_'),
    description: unique.generate('String_')
};

describe('Sna type', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Sna type must be created', function (done) {
        client.snapp.snaType.create(data, done);
    });

    it('Sna type must be checked', function (done) {
        client.snapp.snaType.checkValues(data.model, data, done);
    });

    it('Sna type must be updated', function (done) {
        client.snapp.snaType.edit(data.model, data2, done);
    });

    it('Sna type must be checked', function (done) {
        client.snapp.snaType.checkValues(data2.model, data2, done);
    });

    it('Sna type must be deleted', function (done) {
        client.snapp.snaType.delete(data2.model, function(){
            client.search("", function(){
                client.waitForDataTableWasLoaded(done);
            })
        });
    });

    it('Sna type must be checked for deletion', function (done) {
        client.snapp.common.searchAndCheckIsTableRowWithTextExist(data2.model, function(isExist){
            client.assert(!isExist, 'Sna type is not deleted.');
            done()
        });
    });
});
