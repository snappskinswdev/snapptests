var assert = require("assert");
var unique = global.loadHelper('unique');
var client = global.client;

var snaTypeData = {
    'model': unique.generate("Model_"),
    'manufacturer': unique.generate("Manufacture_")
};

var snaOptions = {
    'number': unique.generate('SerialNumber_'),
    typeString: client.snapp.snaType.generateTypeString(snaTypeData)
};
var commandOptions = {
    'commandText': 'Care plan is changed'
};


describe('Command', function() {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Pre tests', function (done) {
        client.snapp.snaType.create(snaTypeData, function () {
            client.snapp.sna.create(snaOptions, function () {
                done();
            });
        });
    });

    it('Create new command', function (done) {

        client.clickOnMenu('#commands', function () {

            client.waitForExist('#new-button', 5000, false, function (err, res) {
                client.assert(res);

                client
                    .click('#new-button')
                    .waitForExist('#save-popup-button', 3000, false, function (err, res) {
                        client.assert(res);

                        client
                            .selectByVisibleText('#device', snaOptions.number)
                            .selectByVisibleText('select[name="command_id"]', commandOptions.commandText)
                            .click('#save-popup-button', function () {

                                client.waitForPopupErrorText(3000, false, function (err, res) {
                                    client.assert(res, "Error text was not appeared.");

                                    client.cancelPopup(function () {
                                        done();
                                    });
                                })
                            })
                    });
            });
        })
    });

    it('Post tests', function (done) {
        client.snapp.sna.delete(snaOptions.number, function () {
            client.snapp.snaType.delete(snaTypeData.model, done);
        });
    });
});