var assert = require("assert"),
    unique = global.loadHelper("unique"),
    client = global.client;

var accessGroupName = unique.generate('Access group name: ');
var checkBoxId = '#checkbox_112';
var filterTemplateName = unique.generate('Template_');

describe('Access group', function() {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Access group must be created', function (done) {
        client.clickOnMenu('#accessgroups', function () {
            client
                .waitForExist('#edit-fields', 3000, false, function (err, res) {
                    client.assert(res);
                    client
                        .click('#new-button')
                        .waitForExist('#save-button', 2000, false, function (err, res) {
                            client.assert(res);
                            client
                                .setValue('[name="name"]', accessGroupName)
                                .click('label[for="checkbox_112"]')
                                .waitForChecked(checkBoxId, 4000, true, function (err, res) {
                                    client.assert(res);
                                    client
                                        .click('#save-button')
                                        .waitForExist('#data-container > div> table', 9000, false, function (err, res) {
                                            client.assert(res);
                                            client
                                                .setValue('#search [type="text"]', accessGroupName)
                                                .waitForExist('//td[text()="' + accessGroupName + '"]', 5000, false, function (err, res) {
                                                    client.assert(res);
                                                    done();
                                                });
                                        })
                                })
                            ;
                        });
                });
        })
    });

    it('Access group must be checked', function (done) {
        var client = global.client;

        client
            .click('//td[text()="' + accessGroupName + '"]')
            .click('#edit-button')
            .waitForExist(checkBoxId, 3000, false, function (err, res) {
                client.assert(res);
                client.isSelected(checkBoxId, function (err, value, res) {
                    client.assert(res);
                    client.assert(value === false);
                    client.
                        click('#back-button')
                        .waitForExist('#data-container > div > table', 9000, false, function (err, res) {
                            client.assert(res);
                            client
                                .setValue('#search [type="text"]', accessGroupName)
                                .waitForExist('//td[text()="' + accessGroupName + '"]', 3000, false, function (err, res) {
                                    client.assert(res);
                                    done();
                                });
                        })
                });
            });
    });

    it('Filter template of access group must be created', function (done) {
        var client = global.client;

        client
            .click('//td[text()="' + accessGroupName + '"]')
            .click('#edit-fields')
            .waitForExist('#import-button', 10000, false, function (err, res) {
                client.assert(res);
                client
                    .click('a[data-target="fields_project"]')
                    .waitForVisible('label[for="0605_protocol_1"]', 10000, false, function (err, res) {
                        client.assert(res);
                        client
                            .click('label[for="0605_protocol_1"]')
                            .click('label[for="0606_rx_number_2"]')
                            .click('label[for="0609_staff_3"]')
                            .click('button.save-fields-button[data-group="project"]')
                            .waitForVisible('#appModal', 10000, false, function (err, res) {
                                client.assert(res);
                                client
                                    .setValue("[name='name']", filterTemplateName)
                                    .click("#save-popup-button")
                                    .waitForVisible('#appModal', 10000, true, function (err, res) {
                                        client.assert(res);
                                        client
                                            .isSelected("//select[@name='project']/option[text()='" + filterTemplateName + "']", function (err, value, res) {
                                                client.assert(res);
                                                client.assert(value);
                                                client
                                                    .click("#save-button")
                                                    .pause(5000)
                                                    .click("#back-button")
                                                    .waitForExist('#data-container > div> table', 10000, false, function (err, res) {
                                                        client.assert(res);
                                                        client
                                                            .setValue('#search [type="text"]', accessGroupName)
                                                            .waitForExist('//td[text()="' + accessGroupName + '"]', 10000, false, function (err, res) {
                                                                client.assert(res);
                                                                done();
                                                            })
                                                    })
                                                ;
                                            });
                                    })

                            })
                        ;
                    });
            })
    });

    it('Filter template of access group must be deleted', function (done) {
        var neededOptionSelector = "//select[@name='project']/option[text()='" + filterTemplateName + "']";
        client
            .click('//td[text()="' + accessGroupName + '"]')
            .click('#edit-fields')
            .waitForExist('#import-button', 10000, false, function (err, res) {
                client.assert(res);
                client.getAttribute(neededOptionSelector, 'value', function (err, val) {

                    client
                        .selectByValue('select[name="project"]', parseInt(val), function(err){
                            if (err) throw err;

                            client.isSelected(neededOptionSelector, function (err, value, res) {
                                client.assert(res);
                                client.assert(value);
                                client
                                    .click('button.delete-button[data-group="project"]')
                                    .waitForConfirmationPopup(3000, false, function (err, res) {
                                        client.assert(res);
                                        client.confirmPopup(function(){

                                            client
                                                .isSelected(neededOptionSelector, function (err, value, res) {
                                                    client.assert(res);
                                                    client.assert(value);
                                                    client
                                                        .clickOnSaveButton(function(){
                                                            client.pause(2000);

                                                            client.clickOnBackButton(function(){
                                                                client.pause(2000);

                                                                client.search(accessGroupName, function(){

                                                                    client.waitForExists('//td[text()="' + accessGroupName + '"]', 5000, function(res){
                                                                        client.assert(res);
                                                                        done();
                                                                    });
                                                                });
                                                            })
                                                        })
                                                    ;
                                                });
                                        });
                                    })
                                ;
                            });
                        })
                });
            })
    });

    it('Access group must be deleted', function (done) {
        client
            .waitForDataTableWasLoaded(10000, function () {
                client
                    .search(accessGroupName, function () {
                        client
                            .click('//td[text()="' + accessGroupName + '"]')
                            .clickOnDeleteButton(function(){

                                client.waitForConfirmationPopup(3000, false, function(err, res){
                                    client.assert(res, "Confirmation popup was not disappeared.");

                                    client.confirmPopup(function(){
                                        client.waitForSuccessPopup(3000, false, function(err, res){
                                            client.assert(res, "Success popup was not appeared.");

                                            client.waitForNotExists('//td[text()="' + accessGroupName + '"]', 3000, function(res) {
                                                client.assert(res, "Access group was not removed.");
                                                done()
                                            });
                                        })
                                    })
                                });
                            })
                    })
            })
    });
});