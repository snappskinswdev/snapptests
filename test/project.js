var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');
var client = global.client;

var protocolOptions = {
    'number': unique.generate('Number_'),
    'name': unique.generate('Name_')
};

var projectData = {
    'rxNumber': unique.generate('Rx_'),
    'description': unique.generate('Description_'),
    'startDateTime': unique.generateDate(),
    'periodData': unique.generateNumbers(2),
    protocolString: client.snapp.protocol.generateProtocolSelectString(protocolOptions)
};

var projectData2 = {
    'rxNumber': unique.generate('Rx_'),
    'description': unique.generate('Description_'),
    'startDateTime': unique.generateDate(),
    'periodData': unique.generateNumbers(2),
    protocolString: client.snapp.protocol.generateProtocolSelectString(protocolOptions)
};

describe('Projects', function() {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    before('Create environment', function (done) {
        client.snapp.protocol.create(protocolOptions, done);
    });

    it('Project must be created', function (done) {
        client.snapp.project.create(projectData, done);
    });

    it('Project must be checked', function (done) {
        client.snapp.project.checkValues(projectData.rxNumber, projectData, done);
    });

    it('Project must be edited', function (done) {
        client.snapp.project.edit(projectData.rxNumber, projectData2, done);
    });

    it('Project must be checked', function (done) {
        client.snapp.project.checkValues(projectData2.rxNumber, projectData2, done);
    });

    it('Project must be deleted', function (done) {
        client.snapp.project.delete(projectData2.rxNumber, done)
    });

    after('Remove environment', function (done) {
        client.snapp.protocol.delete(protocolOptions.number, done);
    });
});

