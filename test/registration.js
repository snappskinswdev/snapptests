var assert = require("assert");
var async = require("async");
var unique = global.loadHelper('unique');

var protocolData = {
    'number': unique.generate('Number_'),
    'name': unique.generate('Name_')
};

var snaTypeData = {
    'model': unique.generate("Model_"),
    'manufacturer': unique.generate("Manufacture_")
};
var snaTypeData2 = {
    'model': unique.generate("Model2_"),
    'manufacturer': unique.generate("Manufacture2_")
};

var snaData = {
    'number': unique.generate('Number_'),
    'location': unique.generate('Location_'),
    'typeString': client.snapp.snaType.generateTypeString(snaTypeData.manufacturer, snaTypeData.model)
};
var snaData2 = {
    'number': unique.generate('Number_'),
    'location': unique.generate('Location_'),
    'typeString': client.snapp.snaType.generateTypeString(snaTypeData2.manufacturer, snaTypeData2.model)
};
var projectData = {
    'rxNumber': unique.generate('Rx_'),
    'description': unique.generate('Description_'),
    'startDateTime': '01/01/2015',
    'endDateTime': '01/02/2015',
    'periodData': '99',
    protocolString: client.snapp.protocol.generateProtocolSelectString(protocolData)
};
var projectData2 = {
    'rxNumber': unique.generate('Rx_'),
    'description': unique.generate('Description_'),
    'startDateTime': '01/01/2015',
    'endDateTime': '01/02/2015',
    'periodData': '99',
    protocolString: client.snapp.protocol.generateProtocolSelectString(protocolData)
};
var patientData = {
    'ssn': unique.generateSsn(),
    'patientId': unique.generate('PatientId_'),
    'firstName': unique.generate('FirstName_'),
    'lastName': unique.generate('LastName_'),
    'birthday': '01/01/2001',
    'phone': unique.generatePhone(),
    'email': unique.generate('') + unique.generate('@') + '.com'
};

var checkProjectViewForValues = function(projectData, patientData, snaData, callback) {
    client.snapp.project.openEditPage(projectData.rxNumber, function () {
        async.series(
            [
                function (cb) {
                    client.getValue('[name="start_date_time"]', function (err, text) {
                        assert.equal(text, projectData.startDateTime, 'Start date was not saved.');
                        cb();
                    });
                },
                function (cb) {
                    client.getValue('[name="period_data"]', function (err, text) {
                        assert.equal(text, projectData.periodData, 'Period data was not saved.');
                        cb();
                    });
                },
                function (cb) {
                    var patientString = patientData.patientId + ' : ' + patientData.firstName + ' ' + patientData.lastName;
                    client.getValue('#patient-data', function (err, text) {
                        assert.equal(text, patientString, 'Patient was not attached to this project.');
                        cb();
                    });
                },
                function (cb) {
                    var searchSnaString = '//td[text()="' + snaData.number + '"]';
                    client.isExisting(searchSnaString, function(err, isExisting) {
                        assert(isExisting, 'Sna was not attached to this project.');
                        cb();
                    });
                }
            ],
            function (err) {
                if (err) throw err;
                client.clickOnBackButton(function () {
                    client.waitForDataTableWasLoaded(callback)
                });
            }
        );
    })
};


describe('Registration', function () {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    before('Create environment', function (done) {
        client.snapp.protocol.create(protocolData, function () {
            client.snapp.snaType.create(snaTypeData, function () {
                client.snapp.snaType.create(snaTypeData2, function () {
                    client.snapp.sna.create(snaData, function () {
                        client.snapp.sna.create(snaData2, function () {
                            done();
                        });
                    });
                });
            });
        });
    });

    it('Check in. Patient must be registered.', function (done) {

        var patientFormElementNames = client.snapp.patient.formElementNames;

        client.clickOnRegistrationMenu('#registration/checkin', function () {

            client.waitForExists(client.snapp.buttons.checkin.selector, 5000, function (res) {
                assert(res, "Checkin button was not appeared.");

                client
                    .selectByVisibleText('[name="location"]', snaData.location)
                    .setTextInputValueByName(patientFormElementNames.ssn, patientData.ssn)
                ;

                client.click("#find-patient", function () {
                    client.waitForVisible('[name="patient_id"]', 3000, false, function (err, res) {
                        assert(res, "Patient details were not appeared.");

                        client.getText("div.patient-toggle-not-found", function (err, text) {
                            assert(text !== '', "Patient with this ssn has already exists");

                            var endOfTest = function(){
                                client.snapp.project.fillEditForm(projectData, function() {
                                    client.clickOnCheckinButton(function(){
                                        client.waitForSuccessPopupAppeared(done);
                                    })
                                });
                            };

                            if (text == "") { //patient exists
                                endOfTest();
                            } else {
                                client.snapp.patient.fillEditForm(patientData, endOfTest);
                            }

                        });
                    });
                });
            });
        })
    });

    it('Project must be checked for correct values.', function (done) {
        checkProjectViewForValues(projectData, patientData, snaData, function () {
            done();
        });
    });

    it('Change location. Current location of patient must be changed.', function (done) {

        client.clickOnRegistrationMenu('#registration/change-location', function () {

            client.waitForExists(client.snapp.buttons.changeLocation.selector, 5000, function (res) {
                assert(res, "Change location button was not appeared.");

                client
                    .selectByVisibleText('[name="used_location"]', snaData.location)
                    .selectByVisibleText('[name="location"]', snaData2.location)
                ;

                client.waitForExists('//h3[text()="Patient:"]', 6000, function (res) {
                    assert(res, "Patient details were not appeared.");

                    client.snapp.project.fillEditForm(projectData2, function() {
                        client.clickOnChangeLocationButton(function(){
                            client.waitForSuccessPopupAppeared(done);
                        })
                    });
                });
            });
        })
    });

    /*it('Old project must be checked for correct values.', function (done) {
        client.openEditProjectView(projectData.rxNumber, {}, function () {

            client.isTableRowWithTextExist(snaData.number, function(isExist){
                assert(!isExist, 'Sna was not removed from old project.');

                client.clickOnBackButton(function () {
                    client.waitForDataTableWasLoaded(done)
                });
            });
        })
    });*/ //todo test fails because old project does not display in project list. To find this project press archived checkbox

    it('New project must be checked for correct values.', function (done) {
        checkProjectViewForValues(projectData2, patientData, snaData2, function () {
            done();
        });
    });

    it('Checkout. Patient must be checked out.', function (done) {
        var client = global.client;

        client.clickOnRegistrationMenu('#registration/checkout', function () {

            client.waitForExists(client.snapp.buttons.checkout.selector, 5000, function (res) {
                assert(res, "Checkout button was not appeared.");

                client
                    .selectByVisibleText('[name="location"]', snaData2.location)
                ;

                client.waitForExists('//h3[text()="Patient:"]', 6000, function (res) {
                    assert(res, "Patient details were not appeared.");

                    client.clickOnCheckoutButton(function () {

                        client.waitForConfirmationPopupAppeared(function () {
                            client.confirmPopup(function () {
                                client.waitForSuccessPopupAppeared(done)
                            });
                        })
                    })
                });
            });
        })
    });

    /*it('New project must be checked for correct values.', function (done) {
        var client = global.client;

        client.openEditProjectView(projectData.rxNumber, {}, function () {

            client.isTableRowWithTextExist(snaData2.number, function(isExist){
                assert(!isExist, 'Sna was not removed from old project.');

                client.clickOnBackButton(function () {
                    client.waitForDataTableWasLoaded(done)
                });
            });
        })
    });*///todo test fails because old project does not display in project list. To find this project press archived checkbox

    after('Remove environment', function (done) {
        client.snapp.project.deleteArchived(projectData.rxNumber, function () {
            client.snapp.project.delete(projectData2.rxNumber, function () {
                client.snapp.patient.delete(patientData.patientId,function () {
                    client.snapp.sna.delete(snaData.number, function () {
                        client.snapp.sna.delete(snaData2.number, function () {
                            client.snapp.snaType.delete(snaTypeData2.model, function () {
                                client.snapp.snaType.delete(snaTypeData.model, function () {
                                    client.snapp.protocol.delete(protocolData.number, done);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});