var argv = require('optimist').argv,
    assert = require("assert"),
    fs = global.loadHelper("filesystem");

before(function (done) {
    this.timeout(global.config.SUIT_TIMEOUT);

    fs.removeDirSync(global.config.SCREENSHOT_FOLDER, false, ['.gitkeep']);

    global.server.run().then(function () {
        global.client.init(function(){

            global.client
                .url(argv.url)
                .title(function (err, res) {
                    assert('Snappskin' === res.value);

                    global.client
                        .waitFor('#login', 3000)
                        .click('#login')
                        .setValue('#login-email', argv.login || global.config.DEFAULT_USERNAME)
                        .setValue('#login-password', argv.password || global.config.DEFAULT_PASSWORD)
                        .click('#login')
                        .waitForExist('#logout-button', 3000, false, function (err, res) {
                            assert(res);
                            done();
                        })
                });
        });


    });
});