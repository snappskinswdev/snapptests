var assert = require("assert");
var unique = global.loadHelper('unique');

var planName = unique.generate('Test care plan: ');

describe('Care plan', function () {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Care plan must be created', function (done) {
        client.clickOnMenu('#care-plan', function () {
            client.waitForExist('#new-button', 3000, false, function (err, res) {
                client.assert(res);
                client.click('#new-button', function () {
                    client.waitForExist('#save-popup-button', 2000, false, function (err, res) {
                        client.assert(res);
                        client
                            .setValue('[name="name"]', planName)
                            .setValue('[name="periodValue"]', 2)
                            .selectByValue('[name="periodType"]', 2)
                            .savePopup(function () {
                                client.waitForSuccessPopup(3000, true, function (err, res) {
                                    client.assert(res, "Care plan was saved with errors.");

                                    client.waitForExist("#new-button", 3000, false, function (err, res) {
                                        client.assert(res, "Care plan list page is not appeared.");

                                        done();
                                    });
                                });
                            })
                    });
                })
            });
        });
    });

    it('Edit Care Plan', function (done) {
        client
            .click('//td[text()="' + planName + '"]')
            .click('#edit-button')
            .waitForVisible('button[data-action="add"]', 3000, false, function (err, res) {
                client.assert(res);
                client
                    .click('button[data-action="add"]')
                    .waitForExist('#close-button', 3000, false, function (err, res) {
                        client.assert(res);
                        client
                            .selectByValue('[name="vitals"]', 'activity')
                            .selectByValue('[name="timing-type"]', 'list')
                            .selectByValue('[name="setup"]', 'day-night')
                            .selectByValue('[name="threshold-depth"]', 'measurements')
                            //.setValue('.day-picker-container input', 24)
                            .selectByValue('[name="threshold-check"]', 'absolute-value')
                            .selectByValue('[name="periodType"]', 2)
                            .setValue('div[data-value-name="alarmValues"] input[name="low"]', 1)
                            .setValue('div[data-value-name="alarmValues"] input[name="high"]', 4)
                            .setValue('div[data-value-name="alertValues"] input[name="low"]', 2)
                            .setValue('div[data-value-name="alertValues"] input[name="high"]', 5)
                            .setValue('div[data-value-name="thresholdDepth"] input', 5, function (a, b, c) {
                                client.click('#close-button')
                                    .waitForExist('//td[text()="Activity"]', 3000, false, function (err, res) {
                                        client.assert(res);
                                        client.click('button[data-action="saveTemplate"]', function () {
                                            client.waitForSuccessPopup(3000, false, function (err, res) {
                                                client.assert(res);
                                                client.click('#back-button', function () {
                                                    client.waitForExist('.tool.search', 5000, false, function (err, res) {
                                                        client.assert(res);
                                                        client
                                                            .setValue('.tool.search input[type="text"]', planName)
                                                            .waitForExist('//td[text()="' + planName + '"]', 5000, false, function (err, res) {
                                                                client.assert(res);
                                                                client
                                                                    .click('//td[text()="' + planName + '"]')
                                                                    .click('#edit-button')
                                                                    .waitForExist('//td[text()="Activity"]', 3000, false, function (err, res) {
                                                                        client.assert(res);
                                                                        client
                                                                            .click('#back-button')
                                                                            .waitForVisible('.tool.search', 3000, false, function (err, res) {
                                                                                client.assert(res);
                                                                                done();
                                                                            });
                                                                    });
                                                            });
                                                    });
                                                });
                                            })
                                        })
                                    });
                            })

                    });
            })
    });

    it('Care plan must be found and deleted', function (done) {
        var client = global.client;
        var planSelector = '//td[text()="' + planName + '"]';
        client
            .search(planName, function () {
                client.waitForExist(planSelector, 3000, false, function (err, res) {
                    client.assert(res);
                    client.click(planSelector, function (err) {
                        if (err) throw err;

                        client.click('#delete-button', function (err) {
                            if (err) throw err;

                            client.waitForConfirmationPopup(3000, false, function (err, res) {
                                client.assert(res);

                                client.click('#confirmation', function () {
                                    client.waitForConfirmationPopup(3000, true, function (err, res) {
                                        client.assert(res);

                                        client.isExisting(planSelector, function (err, res) {
                                            client.assert(!res, 'Care plan was not deleted.');

                                            done();
                                        });

                                    })
                                })
                            });
                        })
                    })
                });
            })
    });
});