var assert = require("assert");
var client = global.client;
var unique = global.loadHelper('unique');

var data = {
    name: unique.generate('String_'),
    description: unique.generate('String_')
};
var data2 = {
    name: unique.generate('String_'),
    description: unique.generate('String_')
};

describe('Roles', function () {
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('New role must be created', function (done) {
        client.snapp.role.create(data, done);
    });

    it('New role must be checked', function (done) {
        client.snapp.role.checkValues(data.name, data, done);
    });

    it('New role must be updated', function (done) {
        client.snapp.role.edit(data.name, data2, done);
    });

    it('New role must be checked', function (done) {
        client.snapp.role.checkValues(data2.name, data2, done);
    });

    it('New role must be deleted', function (done) {
        client.snapp.role.delete(data2.name, done);
    });
});


