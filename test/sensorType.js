var assert = require("assert");
var unique = global.loadHelper('unique');
var client = global.client;

var data = {
    model: unique.generate('String_'),
    fetchRate: unique.generateNumbers(2),
    manufacturer: unique.generate('String_'),
    description: unique.generate('String_')
};
var data2 = {
    model: unique.generate('String_'),
    fetchRate: unique.generateNumbers(2),
    manufacturer: unique.generate('String_'),
    description: unique.generate('String_')
};

describe('Sensor type', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Sensor type must be created', function (done) {
        client.snapp.sensorType.create(data, done);
    });

    it('Sensor type must be checked', function (done) {
        client.snapp.sensorType.checkValues(data.model, data, done);
    });

    it('Sensor type must be updated', function (done) {
        client.snapp.sensorType.edit(data.model, data2, done);
    });

    it('Sensor type must be checked', function (done) {
        client.snapp.sensorType.checkValues(data2.model, data2, done);
    });

    it('Sensor type must be deleted', function (done) {
        client.snapp.sensorType.delete(data2.model, function(){
            client.search("", function(){
                client.waitForDataTableWasLoaded(done);
            })
        });
    });

    it('Sensor type must be checked for deletion', function (done) {
        client.snapp.common.searchAndCheckIsTableRowWithTextExist(data2.model, function(isExist){
            client.assert(!isExist, 'Sensor type is not deleted.');
            done()
        });
    });
});
