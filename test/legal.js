var assert = require("assert");
var client = global.client;

describe('Legal files', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Upload new legal document', function (done) {
        client
            .click('#legal')
            .waitForVisible('#legal.active', 3000, false, function (err, res) {
                client.assert(res);
                client
                    .click('#new-button')
                    .waitForExist('#save-popup-button', 3000, false, function (err, res) {
                        client.assert(res);
                        client
                            .setValue('[name="description"]', 'Test_legal_file_description')
                            .chooseFile('#fileUpload', 'test/legal/test.txt')
                            .click('#save-popup-button')
                                .waitForExist('//td[text()="Test_legal_file_description"]', 5000, false, function (err, res) {
                                    client.assert(res);
                                    done();
                                });
                    });
            });
    });

    it('Delete uploaded legal document', function (done) {
        client
            .click('//td[text()="Test_legal_file_description"]')
            .click('#delete-button')
            .waitForVisible('#confirmation', 5000, false, function (err, res) {
                client.assert(res);
                client
                    .click('#confirmation')
                    .waitForExist('//td[text()="Test_legal_file_description"]', 3000, true, function (err, res) {
                        client.assert(res);
                        done();
                    });
            });

    });

});





