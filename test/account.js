var assert = require("assert");
var unique = global.loadHelper('unique');

var accountData = {
    firstName: unique.generate('FirstName_'),
    lastName: unique.generate('LastName_')
};

var newPassword = unique.generate('SnappSkin&1_');
var accountData2 = {
    firstName: unique.generate('FirstName_'),
    lastName: unique.generate('LastName_'),
    employeeNumber: unique.generate('EmployeeNumber_'),
    password: newPassword,
    passwordConfirm: newPassword
};

describe('Account', function(){
    this.timeout(global.config.SUIT_TIMEOUT);
    this.bail(true);

    it('Account must be created', function (done) {
        client.snapp.account.create(accountData, done);
    });

    it('Account must be checked', function (done) {
        client.snapp.account.checkValues(accountData.lastName, accountData, done);
    });

    it('Account must be updated', function (done) {
        client.snapp.account.edit(accountData.lastName, accountData2, done);
    });

    it('Account must be checked', function (done) {
        client.snapp.account.checkValues(accountData2.lastName, accountData2, done);
    });

    it('Account must be deleted', function (done) {
        client.snapp.account.delete(accountData2.lastName, done);
    });
});