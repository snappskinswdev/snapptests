var fs = require('fs');
var http = require('http');
var fileName = 'selenium-server-standalone.jar'
try {
  stats = fs.statSync(fileName);
}
catch (e) {
  http.get('http://selenium-release.storage.googleapis.com/2.45/selenium-server-standalone-2.45.0.jar', function (response) {
    response.pipe(fs.createWriteStream(fileName));
  });
}