var server = require('./server.js'),
    assert = require("assert")
    ;

global.HELPERS_PATH = __dirname + '/helpers';
global.loadHelper = function(helperName) {
    return require(global.HELPERS_PATH + '/' + helperName);
};

global.server = server;

global.config = {
    SUIT_TIMEOUT: 99999,
    DEFAULT_USERNAME: 'SnappskinAdmin',
    DEFAULT_PASSWORD: 'SnappAdmin&1',
    SCREENSHOT_FOLDER: __dirname + '/testScreenshots',

    client: {
        patient: {
            ssnSize: 9
        }
    }
};


var client = require('webdriverio').remote({
    desiredCapabilities: {
        browserName: 'firefox'
    }
});
global.client = global.loadHelper('client').extend(client);